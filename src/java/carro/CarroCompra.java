/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import entidad.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Armand
 */
public class CarroCompra 
{
    //el carro tiene varias instancias de ItemCarroCompra, so crear una Lista.
    List<ItemCarroCompra> muebles;
    int numMuebles;
    double total;
    
    public CarroCompra()
    {
        muebles=new ArrayList<ItemCarroCompra>();
        numMuebles=0;
        total=0;
    }
    
    public synchronized void addMueble(Mueble mueble) 
    {
        boolean newMueble = true;

        for (ItemCarroCompra ccm : muebles) 
        {

            if (Objects.equals(ccm.getMueble().getIdMueble(), mueble.getIdMueble())) 
            {
                newMueble = false;
                ccm.subirCantidad();
            }
        }
        if (newMueble) 
        {
            ItemCarroCompra ccm = new ItemCarroCompra (mueble);
            muebles.add(ccm);
        }
    }
    
    public synchronized void actualiza(Mueble mueble, String cantidad) 
    {
        short cant = -1;
        cant = Short.parseShort(cantidad);

        if (cant >= 0) 
        {
            ItemCarroCompra muebl = null;

            for (ItemCarroCompra ccm : muebles) 
            {
                if (Objects.equals(ccm.getMueble().getIdMueble(), mueble.getIdMueble())) 
                {
                    if (cant != 0) 
                    {
                        ccm.setCantidad(cant);
                    }
                    else
                    {
                        muebl = ccm;
                        break;
                    }
                }
            }
            if (muebl != null) 
            {
                muebles.remove(muebl);
            }
        }
    }
    
    public synchronized List<ItemCarroCompra> getMuebles() 
    {
        return muebles;
    }
    
    public synchronized int getNumMuebles() 
    {
        numMuebles = 0;
        for (ItemCarroCompra ccm : muebles) 
        {

            numMuebles += ccm.getCantidad();
        }
        return numMuebles;
    }
    
    public synchronized double getSubtotal() 
    {
        double monto = 0;

        for (ItemCarroCompra ccm : muebles) 
        {

            Mueble m = (Mueble) ccm.getMueble();
            monto += (ccm.getCantidad() * m.getPrecio().doubleValue());
        }
        return monto;
    }
    
    public synchronized void calculaTotal(String recargo) 
    {
        double monto = 0;
        double rec = Double.parseDouble(recargo);
        monto = this.getSubtotal();
        monto += rec;
        total = monto;
    }
    
    public synchronized double getTotal() 
    {
        return total;
    }
    
    public synchronized void limpiar() 
    {
        muebles.clear();
        numMuebles = 0;
        total = 0;
    }
    
    
}
