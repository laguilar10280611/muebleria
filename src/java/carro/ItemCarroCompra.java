/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import entidad.*;

/**
 *
 * @author Armand
 */
public class ItemCarroCompra 
{
    Mueble mueble;
    short cantidad;
    
    public ItemCarroCompra(Mueble muebl) 
    {
        mueble = muebl;
        cantidad = 1;
    }

    public Mueble getMueble() 
    {
        return mueble;
    }
    
    public short getCantidad() 
    {
        return cantidad;
    }

    public void setCantidad(short cant) 
    {
        cantidad = cant;
    }

    public void subirCantidad() 
    {
        cantidad++;
    }

    public void bajarCantidad() 
    {
        cantidad--;
    }

    public double getTotal() 
    {
        double monto = 0;
        monto = (this.getCantidad() * mueble.getPrecio().doubleValue());
        return monto;
    }
    
}
