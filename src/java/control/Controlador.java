/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import carro.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entidad.*;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpSession;
import sesion.*;
import validaciones.Valida;

/**
 *
 * @author Armand
 */
@WebServlet(name="Controlador", loadOnStartup=1, urlPatterns={"/Categoria","/addCarro","/verCarro","/actualizaCarro","/Pago","/Confirmacion"})
public class Controlador extends HttpServlet {

    private String recargo;
    
    @Inject
    private CategoriaFacade categoriaF;
    @Inject
    private MuebleFacade muebleF;
    @Inject
    private TiendaMuebles tiendaMueblesF;
    
    @Override
    public void init(ServletConfig servletConfig) throws ServletException
    {
        super.init(servletConfig);
        recargo = servletConfig.getServletContext().getInitParameter("deliverySurcharge");//*
        getServletContext().setAttribute("categorias", categoriaF.findAll());
        System.out.println("SALIENDO DEL CONTROLADOR");
    }
  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
       
        String userPath= request.getServletPath();
        HttpSession session=request.getSession();
        Categoria categoelegida;
        List<Mueble> categoriaMuebles;
        System.out.println("RUTA USUARIO= "+userPath);
        
        if(userPath.equals("/Categoria"))
        {
            String idCategoria=request.getQueryString();
            if(idCategoria!=null)
            {
                categoelegida=categoriaF.find(Short.parseShort(idCategoria));
                session.setAttribute("categoriaSel", categoelegida);
                categoriaMuebles=categoelegida.getMuebleList();
                session.setAttribute("categomuebles",categoriaMuebles);
                 
            } 
        }
        else
        {
            if(userPath.equals("/verCarro"))
            {
                String limpiar = request.getParameter("limpiar");//*
                if ((limpiar != null) && limpiar.equals("true")) //*
                {
                    CarroCompra carro = (CarroCompra) session.getAttribute("carro");
                    carro.limpiar();
                }
                 userPath = "/Carro";//*
                
            }
            else
            {
                if(userPath.equals("/Confirmacion"))
                {
                    CarroCompra carro = (CarroCompra) session.getAttribute("carro");
                    carro.calculaTotal(recargo);
                }
            }
        }
        String url = "/WEB-INF/vista" + userPath + ".jsp";
        try 
        {
            request.getRequestDispatcher(url).forward(request, response);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        request.setCharacterEncoding("UTF-8");
        
        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        CarroCompra carro = (CarroCompra) session.getAttribute("carro");
        Valida verifica = new Valida();
        
        if(userPath.equals("/addCarro"))
        {
            if (carro == null) 
            {
                carro = new CarroCompra();
                session.setAttribute("carro", carro);
            }

            String muebleId = request.getParameter("productId");//*

            if (!muebleId.isEmpty()) 
            {
                Mueble mueble1 = muebleF.find(Integer.parseInt(muebleId));
                carro.addMueble(mueble1);
            }

            userPath = "/categoria";
        }
        else
        {
            if(userPath.equals("/actualizaCarro"))
            {
                String muebleId = request.getParameter("productId");//*
                String cantidad = request.getParameter("quantity");//*

                boolean error = verifica.validaCantidad(muebleId, cantidad);

                if (!error) 
                {

                    Mueble mueble2 = muebleF.find(Integer.parseInt(muebleId));
                    carro.actualiza(mueble2, cantidad);
                }
                userPath = "/carro";
            }
            else
            {
                if(userPath.equals("/Pago"))
                {
                    if (carro != null) 
                    {
                        String nombre = request.getParameter("nombre");//*
                        String email = request.getParameter("email");
                        String telefono = request.getParameter("telefono");
                        String direccion = request.getParameter("direccion");
                        String cp = request.getParameter("cp");
                        String tarjNum = request.getParameter("tarjNum");//*

                        boolean error2 = false;
                        error2 = verifica.validaDatos(nombre, email, telefono, direccion, cp, tarjNum, request);

                        if (error2) 
                        {
                            request.setAttribute("banderaError", error2);
                            userPath = "/Confirmacion";

                        } 
                        else 
                        {
                            int ordenId = tiendaMueblesF.hacerOrden(nombre, email, telefono, direccion, cp, tarjNum, carro);
                            if (ordenId != 0) 
                            {
                                carro = null;
                                session.invalidate();
                                Map ordenMap = tiendaMueblesF.getDetallesOrden(ordenId);
                                
                                request.setAttribute("cliente", ordenMap.get("customer"));//*
                                request.setAttribute("muebles", ordenMap.get("products"));//*
                                request.setAttribute("numOrden", ordenMap.get("orderRecord"));//*
                                request.setAttribute("mueblesEnOrden", ordenMap.get("orderedProducts"));//*

                                userPath = "/confirmation";//*

                            // otherwise, send back to checkout page and display error
                            }
                            else 
                            {
                                userPath = "/Confirmacion";
                                request.setAttribute("errorOrden", true);
                            }
                        }
                    }  
                }
                
            }
        }
        String url = "/WEB-INF/viesta" + userPath + ".jsp";
        try 
        {
            request.getRequestDispatcher(url).forward(request, response);
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
