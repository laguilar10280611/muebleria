/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import entidad.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sesion.*;

/**
 *
 * @author Armand
 */
@WebServlet(name = "Administracion",
            urlPatterns = {"/administracion/",
                           "/administracion/verOrdenes",
                           "/administracion/verClientes",
                           "/administracion/registrarCliente",
                           "/administracion/ordenRegistro",
                           "/administracion/salir"})

@ServletSecurity(
    @HttpConstraint(transportGuarantee = ServletSecurity.TransportGuarantee.CONFIDENTIAL,rolesAllowed = {"affableBeanAdmin"})//* 
)

public class Administracion extends HttpServlet 
{
    @EJB
    private TiendaMuebles tiendaMuebles;
    @EJB
    private ClienteFacade clienteF;
    @EJB
    private OrdenFacade ordenF;

    private String userPath;
    private Cliente cliente;
    private Orden orden;
    private List ordenList = new ArrayList();
    private List clienteList = new ArrayList();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        HttpSession session = request.getSession(true);
        userPath = request.getServletPath();
        
        if (userPath.equals("/administracion/verClientes")) 
        {
            clienteList = clienteF.findAll();
            request.setAttribute("clienteList", clienteList);
        }
        if (userPath.equals("/administracion/verOrdenes")) 
        {
            ordenList = ordenF.findAll();
            request.setAttribute("ordenList", ordenList);
        }
        if (userPath.equals("/administracion/registrarCliente")) 
        {
            String clienteId = request.getQueryString();

            cliente = clienteF.find(Integer.parseInt(clienteId));
            request.setAttribute("registroCliente", cliente);
            orden = ordenF.findByCliente(cliente);
            request.setAttribute("orden", orden);
        }
        if (userPath.equals("/administracion/ordenRegistro")) 
        {
            String ordenId = request.getQueryString();
            Map ordenMap = tiendaMuebles.getDetallesOrden(Integer.parseInt(ordenId));

            request.setAttribute("cliente", ordenMap.get("cliente"));
            request.setAttribute("muebles", ordenMap.get("muebles"));
            request.setAttribute("ordenRegistro", ordenMap.get("ordenRegistro"));
            request.setAttribute("productosPedidos", ordenMap.get("productosPedidos"));
        }
        if (userPath.equals("/administracion/salir")) 
        {
            session = request.getSession();
            session.invalidate();  
            response.sendRedirect("/MuebleriaV2/administracion/");
            return;
        }
        
        userPath = "/administracion/index.jsp";
        try 
        {
            request.getRequestDispatcher(userPath).forward(request, response);
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
