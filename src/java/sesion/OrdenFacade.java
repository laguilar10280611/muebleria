/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Orden;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Armand
 */
@Stateless
public class OrdenFacade extends AbstractFacade<Orden> {
    @PersistenceContext(unitName = "MuebleriaV2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrdenFacade() {
        super(Orden.class);
    }
    
    public Orden find(Object id) 
    {
        Orden orden = em.find(Orden.class, id);
        em.refresh(orden);
        return orden;
    }
    
    @RolesAllowed("affableBeanAdmin")
    public Orden findByCliente(Object cliente)
    {
        return (Orden) em.createNamedQuery("Orden.findByCliente").setParameter("idCliente", cliente).getSingleResult();
    }
    
}
