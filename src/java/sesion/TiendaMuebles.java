/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import carro.*;
import entidad.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Armand
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TiendaMuebles 
{
    @PersistenceContext(unitName = "MuebleriaV2PU")
    private EntityManager em;
    @Resource
    private SessionContext contexto;
    @EJB
    private MuebleFacade muebleF;
    @EJB
    private OrdenFacade ordenF;
    @EJB
    private OrdenmuebleFacade ordenMuebleF;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int hacerOrden(String nombre, String email, String telefono, String direccion, String cp, String tarjNum, CarroCompra carro) 
    {
        try 
        {
            Cliente client = addCliente(nombre, email, telefono, direccion, cp, tarjNum);
            Orden ord = addOrden(client, carro);
            addMueblesOrdenados(ord, carro);
            return ord.getIdOrden();
        } 
        catch (Exception e) 
        {
            contexto.setRollbackOnly();
            return 0;
        }
    }
    
    private Cliente addCliente(String n, String e, String tel, String dir, String cp, String tn) 
    {

        Cliente cliente = new Cliente();
        cliente.setNombre(n);
        cliente.setEmail(e);
        cliente.setTelefono(tel);
        cliente.setDireccion(dir);
        cliente.setCp(cp);
        cliente.setTarjNum(tn);

        em.persist(cliente);
        return cliente;
    }
    
    private Orden addOrden(Cliente client, CarroCompra carro) 
    {
        Orden orden = new Orden();
        orden.setIdCliente(client);
        orden.setTotal(BigDecimal.valueOf(carro.getTotal()));

        Random random = new Random();
        int i = random.nextInt(999999999);
        orden.setNumConfirmacio(i);
        em.persist(orden);
        return orden;
    }
    
    private void addMueblesOrdenados(Orden orden, CarroCompra carro) 
    {
        em.flush();

        List<ItemCarroCompra> muebles = carro.getMuebles();
        for (ItemCarroCompra ccm : muebles) 
        {
            int muebleId = ccm.getMueble().getIdMueble();
            OrdenmueblePK mueblesordPK = new OrdenmueblePK(); 
            mueblesordPK.setIdOrden(orden.getIdOrden());
            mueblesordPK.setIdMueble(muebleId);
            
            Ordenmueble ordmueble = new Ordenmueble(mueblesordPK);

            // set quantity
            ordmueble.setCantidad(ccm.getCantidad());

            em.persist(ordmueble);
        }
    }
    
    public Map getDetallesOrden(int idOrden) 
    {
        Map ordenMap = new HashMap();

        Orden orden = ordenF.find(idOrden);
        Cliente clien = orden.getIdCliente();
        List<Ordenmueble> ordmueble = ordenMuebleF.findByOrderId(idOrden);

        // get product details for ordered items
        List<Mueble> muebles = new ArrayList<Mueble>();

        for (Ordenmueble mo : ordmueble) 
        {

            Mueble m = (Mueble) muebleF.find(mo.getOrdenmueblePK().getIdMueble()); 
            muebles.add(m);
        }

        ordenMap.put("ordenRegistro", orden);
        ordenMap.put("cliente", clien);
        ordenMap.put("mueblesPedidos", ordmueble);
        ordenMap.put("muebles", muebles);

        return ordenMap;
    }
    
    public void businessMethod() 
    {
    }

}
