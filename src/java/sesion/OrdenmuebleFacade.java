/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Ordenmueble;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Armand
 */
@Stateless
public class OrdenmuebleFacade extends AbstractFacade<Ordenmueble> {
    @PersistenceContext(unitName = "MuebleriaV2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrdenmuebleFacade() {
        super(Ordenmueble.class);
    }
    public List<Ordenmueble> findByOrderId(Object id) 
    {
        return em.createNamedQuery("Ordenmueble.findByIdOrden").setParameter("idOrden", id).getResultList();
    }
    
}
