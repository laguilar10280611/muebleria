<%-- 
    Document   : Carro
    Created on : 27/11/2014, 11:32:40 PM
    Author     : Armand
--%>

<%@page pageEncoding="UTF-8"%>

<c:set var="ver" value="/carro" scope="session"/>

<c:choose>
    <c:when test="${carro.numMuebles > 1}">
        <p><fmt:message key="contenidoCarro"/> ${cart.numMuebles} <fmt:message key="muebles"/>.</p>
    </c:when>
    <c:when test="${carro.numMuebles == 1}">
        <p><fmt:message key="contenidoCarro"/> ${carro.numMuebles} <fmt:message key="mueble"/>.</p>
    </c:when>
    <c:otherwise>
        <p><fmt:message key="carroVacio"/></p>
    </c:otherwise>
</c:choose>

<c:if test="${!empty carro && carro.numMuebles != 0}">
    <c:url var="url" value="verCarro">
        <c:param name="limpiar" value="true"/>
    </c:url>

    <a href="${url}"><fmt:message key="limpiarCarro"/></a>
</c:if>

<c:set var="valor">
    <c:choose>        
        <c:when test="${!empty categoElegida}">
            categoria
        </c:when>
        <c:otherwise>
            index.jsp
        </c:otherwise>
    </c:choose>
</c:set>

<c:url var="url" value="${valor}"/>
<a href="${url}"><fmt:message key="continuarComprando"/></a>

<c:if test="${!empty carro && carro.numMuebles != 0}">
        <a href="<c:url value='Confirmacion'/>" ><fmt:message key="pasarConfirmar"/></a>
</c:if>
        
<c:if test="${!empty carro && carro.numMuebles != 0}"> 
    
<h4 id="subtotal"><fmt:message key="subtotal"/>:
    <fmt:formatNumber type="currency" currencySymbol="&dollar; " value="${carro.subtotal}"/>
</h4>

<table>
    <tr>
        <th><fmt:message key="mueble"/></th>
        <th><fmt:message key="nombre"/></th>
        <th><fmt:message key="precio"/></th>
        <th><fmt:message key="cantidad"/></th>
    </tr>

        <c:forEach var="carroMueble" items="${carro.muebles}" varStatus="iter">

          <c:set var="mueble" value="${carroMueble.mueble}"/>

          <tr>
            <td>
                <img src="${initParam.rutaImgMueble}${mueble.nombre}.png" alt="<fmt:message key="${product.name}"/>">     
            </td>

            <td><fmt:message key="${mueble.nombre}"/></td>

            <td>
                <fmt:formatNumber type="currency" currencySymbol="&dollar; " value="${carroMueble.total}"/>
                <br>
                <span>(
                    <fmt:formatNumber type="currency" currencySymbol="&dollar; " value="${mueble.precio}"/>
                    / <fmt:message key="unit"/> )
                </span>
            </td>

            <td>
                <form action="<c:url value='actualizaCarro'/>" method="POST">
                    <input type="hidden"
                           name="muebleId"
                           value="${mueble.idMueble}">
                    <input type="text"
                           maxlength="2"
                           size="2"
                           value="${carroMueble.cantidad}"
                           name="cantidad"
                           style="margin:5px">
                    <input type="submit"
                           name="enviar"
                           value="<fmt:message key='actualiza'/>">
                </form>
            </td>
          </tr>

        </c:forEach>

      </table>


</c:if>

